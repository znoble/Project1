#!usr/bin/python3
import sys
import re
import os
import numpy as np

pat = re.compile("\w+", re.IGNORECASE)
b_line = re.compile("^$")
start = re.compile(r"\*{3,}.*START.*PROJECT GUTENBERG")
end = re.compile(r"END.*PROJECT GUTENBERG", re.IGNORECASE)
pg = re.compile(r"PROJECT GUTENBERG", re.IGNORECASE)
TOC = re.compile(r"CONTENTS")
toc = re.compile(r"Contents")
illList = re.compile(r"illustrat(ed|ions*)", re.IGNORECASE)
chap = re.compile(r"CHAPTER|PREFACE|INTRODUCTION|PROLOGUE|CONCLUSION", re.IGNORECASE)
exp = re.compile(r"EXPLANATORY")

def process_book(fname):
    processed_book = ""
    b_line_count = 0
    in_book = False
    is_text = True

    with open(fname, "r") as fd:
        for line in fd:

            if not in_book:                    ## check for the start of the book
                if start.match(line):
                    in_book = True
                continue

            if end.match(line):                ## check for the end of the line
                break

            if not is_text:                    ## do not print while is_text=False
                if b_line.match(line):         ## if is_text=True and
                    b_line_count += 1          ## 2 blank lines are counted
                else:                          ## assume end of section
                    b_line_count = 0           ## make is_text=True and start
                if b_line_count >= 2:          ## start printing again
                    is_text = True
                    b_line_count = 0
                continue


            if toc.search(line) or TOC.search(line):                ## check for the word CONTENTS or Contents
                is_text = False
                continue

            if re.search(r"\[Illustration\]", line):
                continue

            if illList.search(line):                 ## check for illistrations List
                is_text = False
                continue

            if re.match(r"Produced by", line):   ## text between start and toc
                is_text = False
                continue
            if re.match(r"\[Note", line):        ## other notes in text
                is_text = False
                continue
            if exp.search(line):                 ## EXPLANATORY sections
                is_text = False
                continue

            if re.match(r"^\*+", line):         ## Some texts have notes between *** lines
                is_text = False
                continue

            if chap.search(line):               ## skip chapter heading
                continue


            processed_book += line
    return processed_book

def freq(book):
    wc = {}
    cap_words = []
    res = pat.findall(book)
    for w in res:
        if len(w) >= 4:
            if w in wc:
                wc[w] += 1
            else:
                wc[w] = 1
            if re.match("[A-Z][a-z]+", w):
                if w not in cap_words:
                    cap_words.append(w)
    for word in cap_words:
        if word.lower() not in wc:
            del wc[word]
        else:
            del wc[word]
            wc[word.lower()] += 1
    total = len(wc)
    for word in wc:
        wc[word] = 1000.00 * wc[word] / total
    return wc


def check_dir(path_name):
    os.chdir(path_name)
    file_names = []
    for f in os.listdir():
        if ".txt" in f:
            file_names.append(path_name + "/" + f)
    os.chdir("..")
    return file_names


def main():

    author1_books = check_dir("Author1")
    author2_books = check_dir("Author2")
    a1_counts = {}
    a2_counts = {}
    temp = {}

    #author1_books
    for file in author1_books:
        word_freq = freq(process_book(file))
        for word in word_freq:
            if word in a1_counts:##replace with defaultdict
                a1_counts[word].append(word_freq[word])
            else:
                a1_counts[word] = [word_freq[word]]

    for word in a1_counts:
        if len(a1_counts[word]) >= 5:
            temp[word] = a1_counts[word]
    a1_counts = temp.copy()
    a1_counts = dict(sorted(a1_counts.items(), key=lambda words: words[1], reverse=True))

    #author2_books
    for file in author2_books:
        word_freq = freq(process_book(file))
        for word in word_freq:
            if word in a2_counts:##replace with defaultdict
                a2_counts[word].append(word_freq[word])
            else:
                a2_counts[word] = [word_freq[word]]

    for word in a2_counts:
        if len(a2_counts[word]) >= 5:
            temp[word] = a2_counts[word]
    a2_counts = temp.copy()
    a2_counts = dict(sorted(a2_counts.items(), key=lambda words: words[1], reverse=True))

    word_counts = {}
    i = 0 # count the first 100 words
    for word in a1_counts:
        if i >= 100: break
        if word in a2_counts:
            word_counts[word] = a1_counts[word] + a2_counts[word]
            i += 1

    with open("counts.csv", "w") as fout:
        for word in word_counts:
            fout.write("{0}".format(word))
            for num in word_counts[word]:
                fout.write("," + str(num))
            fout.write("\n")

    # save the model data for later display
    modelarr = np.array(list(word_counts.values()))
    np.save("modelarr.npy", modelarr)
    with open("modelwords.txt", "w") as fout:
        fout.write(",".join(list(word_counts.keys())))

if __name__ == '__main__':
    main()
