Project 1 of CS501 at ISU: Text Classification using K-Nearest-Neighbor

How to Use:
	Run setup.py: Reads books from both Author directories, creates a "counts.csv" frequency table of the first 100 words both authors have in common.
	Run guess.py: Reads the samples from the Samples Directory, uses KNN to determine which author each sample is closest to.

