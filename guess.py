#!usr/bin/python3
# 1. read 1 file from sample directory
# 2. compute word_freq for sample of the 100 words in word_counts.csv
# 3. repeat for all books in sample directory
# 4. calc distance from sample vectors and word_counts vectors using pythag thm
# 5. determine which neighbor it is closest to
# 6. display the results
import os
import sys
import re
import numpy as np
import setup
from setup import process_book, freq, check_dir
import csv

def distance(model, sample, size):
    dist = [0]*size #make this able to be changed based on size of data
    for i in range(size):
        sum = 0 # value of the sum of the first 0 squares
        for word in sample: #iterate through the model arrays
            #model[word[i]] = freq of word in book i
            # ((freq in book i of model) - (freq in sample))**2
            sum += (model[word][i] - sample[word])**2
        #at termination: temp is the sum of the squares of the differences
        #         between model freq and count freq of each word in counts
        dist[i] = sum
    return dist

def main():

    ##import the counts.csv file to use as a model
    model = {}
    with open("counts.csv", newline='') as csvfile:
        reader = csv.reader(csvfile)
        for row in reader:
            model[row[0]] = list(map(float, row[1:]))

    ##end import

    ## iterate through samples folder
    ## find the 100 words in model and calc the freq
    ## find distance between each sample and the model
    ## determine 3 closest neighbors
    ## print findings
    samples = check_dir("Samples")

    for file in samples:
        sample_words = freq(process_book(file))
        counts = {}
        for word in sample_words:     #find the word in sample that are in the model
            if word in model:
                counts[word] = sample_words[word]

        dist = distance(model, counts, 10)

        #find the 3 smallest numbers and return the indices
        smallest = sorted(dist)[0:3]
        for i in range(3):
            smallest[i] = dist.index(smallest[i])
        #evaluate for soltuion
        #counters for solution
        author1, author2 = 0, 0
        for i in smallest:
            if i < 5:
                author1 += 1
            else:
                author2 += 1
        if author1 > author2:
            print("{2}: author1 by a vote of {0} to {1} using 3-nearest neighbor".format(author1, author2, file[8:]))
        else:
            print("{2}: author2 by a vote of {0} to {1} using 3-nearest neighbor".format(author1, author2, file[8:]))


    #save sample word counts for display
    samplearr = np.array(list(counts.values()))
    np.save("samplearr.npy", samplearr)
    with open("samplewords.txt", "w") as fout:
        fout.write(",".join(list(counts.keys())))


if __name__ == '__main__':
    main()
